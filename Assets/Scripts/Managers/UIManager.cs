﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Button EndTurnBtn;
    public Text Turn;
    public Text TurnsLeft;

	// Use this for initialization
	void Start ()
    {
        EndTurnBtn.onClick.AddListener(EndTurn);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Grid.Instance.isPlayerTurn)
            Turn.text = "Player";
        else
            Turn.text = "Enemy";

        TurnsLeft.text = Grid.Instance.PlayerChessmansToMove.Count.ToString();
		
	}

    void EndTurn()
    {
        if (GameObject.Find("Grid(Clone)").GetComponent<Grid>().isPlayerTurn)
        {
            GameObject.Find("Grid(Clone)").GetComponent<Grid>().isPlayerTurn = false;
        }
            

    }


}
