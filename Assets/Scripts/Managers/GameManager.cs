﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Grid gridPrefab;

    private Grid gridInstace;

    public int GameMode = 1;


    // Use this for initialization
    void Start () {

        BeginGame();

	}
	
	// Update is called once per frame
	void Update () {

        if (!Grid.Instance.isPlayerTurn)
        {
            print("AI moved nothing");
            //Grid.Instance.isPlayerTurn = true;
            Grid.Instance.AI_Moves();
        }

    }

    private void BeginGame()
    {
        gridInstace = Instantiate(gridPrefab) as Grid;
        gridInstace.Generate();

        gridInstace.SpawnPieces(GameMode);

    }

    private void RestartGame()
    {

        Destroy(gridInstace.gameObject);
        BeginGame();

    }

   


}
