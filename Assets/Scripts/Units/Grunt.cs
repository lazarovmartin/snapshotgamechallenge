﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grunt : Chessman {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];

        Chessman c, c2;

        //Moves
        if (isPlayer)
        {
            //Diagonal Left
            if(CurrentX != 0 && CurrentY != 7)
            {
                c = Grid.Instance.Chessmans[CurrentX - 1, CurrentY + 1];
                if(c != null && !c.isPlayer)
                {
                    r[CurrentX - 1, CurrentY + 1] = true;
                }
            }

            //Diagonal Right
            if (CurrentX != 7 && CurrentY != 7)
            {
                c = Grid.Instance.Chessmans[CurrentX + 1, CurrentY + 1];
                if (c != null && !c.isPlayer)
                {
                    r[CurrentX + 1, CurrentY + 1] = true;
                }
            }

            //Middle - Forward
            if(CurrentY != 7)
            {
                c = Grid.Instance.Chessmans[CurrentX, CurrentY + 1];
                if(c == null)
                {
                    r[CurrentX, CurrentY + 1] = true;
                }
            }

            //Middle on first move - forward
            if(CurrentY == 1)
            {
                c = Grid.Instance.Chessmans[CurrentX, CurrentY + 1];
                c2 = Grid.Instance.Chessmans[CurrentX, CurrentY + 2];
                if(c == null && c2 == null)
                {
                    r[CurrentX, CurrentY + 2] = true;
                }
            }
            
        }


        return r;
    }

    
}
