﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : Chessman {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];


        Chessman c;
        int i, j;

        //Right
        i = CurrentX;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Grid.Instance.Chessmans[i, CurrentY];
            if (c == null)
                r[i, CurrentY] = true;
            else
            {
                if (c.isPlayer != isPlayer)
                    r[i, CurrentY] = true;

                break;
            }
        }

        //Left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i < 0)
                break;

            c = Grid.Instance.Chessmans[i, CurrentY];
            if (c == null)
                r[i, CurrentY] = true;
            else
            {
                if (c.isPlayer != isPlayer)
                    r[i, CurrentY] = true;

                break;
            }
        }

        //UP
        i = CurrentY;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Grid.Instance.Chessmans[CurrentX, i];
            if (c == null)
                r[CurrentX, i] = true;
            else
            {
                if (c.isPlayer != isPlayer)
                    r[CurrentX, i] = true;

                break;
            }
        }

        //Down
        i = CurrentY;
        while (true)
        {
            i--;
            if (i < 0)
                break;

            c = Grid.Instance.Chessmans[CurrentX, i];
            if (c == null)
                r[CurrentX, i] = true;
            else
            {
                if (c.isPlayer != isPlayer)
                    r[CurrentX, i] = true;

                break;
            }
        }

        return r;
    }
}
