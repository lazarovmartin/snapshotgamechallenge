﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpship : Chessman {


    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];

        //UpLeft
        JumpshipMove(CurrentX - 1, CurrentY + 2, ref r);

        //UpRight
        JumpshipMove(CurrentX + 1, CurrentY + 2, ref r);

        //RightUp
        JumpshipMove(CurrentX + 2, CurrentY + 1, ref r);

        //RightDown
        JumpshipMove(CurrentX + 2, CurrentY - 1, ref r);

        //DownLeft
        JumpshipMove(CurrentX - 1, CurrentY - 2, ref r);

        //DownRight
        JumpshipMove(CurrentX + 1, CurrentY - 2, ref r);

        //LeftUp
        JumpshipMove(CurrentX - 2, CurrentY + 1, ref r);

        //LeftDown
        JumpshipMove(CurrentX - 2, CurrentY - 1, ref r);

        return r;

    }

    public void JumpshipMove(int x ,int y, ref bool[,] r)
    {

        Chessman c;

        if(x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = Grid.Instance.Chessmans[x, y];
            if(c == null)
            {
                r[x, y] = true;
            }
        }

    }

}
