﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dreadnought : Chessman {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];

        //UpLeft
        DreadnoughtMove(CurrentX - 1, CurrentY + 1, ref r);

        //UpRight
        DreadnoughtMove(CurrentX + 1, CurrentY + 1, ref r);

        //RightUp
        DreadnoughtMove(CurrentX + 1, CurrentY + 1, ref r);

        //RightDown
        DreadnoughtMove(CurrentX + 1, CurrentY - 1, ref r);

        //DownLeft
        DreadnoughtMove(CurrentX - 1, CurrentY - 1, ref r);

        //DownRight
        DreadnoughtMove(CurrentX + 1, CurrentY - 1, ref r);

        //LeftUp
        DreadnoughtMove(CurrentX - 1, CurrentY + 1, ref r);

        //LeftDown
        DreadnoughtMove(CurrentX - 1, CurrentY - 1, ref r);

        //Up
        DreadnoughtMove(CurrentX, CurrentY + 1, ref r);

        //Down
        DreadnoughtMove(CurrentX, CurrentY - 1, ref r);

        //Left
        DreadnoughtMove(CurrentX - 1, CurrentY, ref r);

        //Right
        DreadnoughtMove(CurrentX + 1, CurrentY, ref r);

        return r;

    }

    public void DreadnoughtMove(int x, int y, ref bool[,] r)
    {

        Chessman c;

        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = Grid.Instance.Chessmans[x, y];
            if (c == null)
            {
                r[x, y] = true;
            }
        }

    }
}
