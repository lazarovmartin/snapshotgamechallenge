﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Chessman : MonoBehaviour {

	public int CurrentX { set; get; }
    public int CurrentY { set; get; }
    public bool isPlayer;

    public int attackPower { set; get; }
    public int hitPoints { set; get; }

    public bool isAttacking;

    public Text HP;

    public void SetPostion(int _x,int _y)
    {

        CurrentX = _x;
        CurrentY = _y;

    }

    public virtual bool[,] PossibleMove()
    {
        return new bool[8,8];
    }

    public bool CanBeSelected { set; get; }

    public Chessman target { set; get; }
}
