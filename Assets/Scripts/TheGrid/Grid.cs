﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    public int sizeX, sizeZ;
    public Tile[,] tiles;
    public Tile tilePrefab;

    public GameObject[] allPiecesPrefabs;

    //public GameObject CommandUnitPrefab;
    //public GameObject DronePrefab;
    //public GameObject DreadnoughtPrefab;

    //public GameObject GruntPrefab;
    //public GameObject JumpshipPrefab;
    //public GameObject TankPrefab;


    private int selectionX = -1;
    private int selectionY = -1;

    public Chessman[,] Chessmans { set; get; }
    private Chessman selectedChessman;

    private List<GameObject> activeChessman;

    public bool isPlayerTurn = true;

    public static Grid Instance { set; get; }
    private bool[,] allowedMoves { set; get; }

    private int playerMovesLeft, playerMaxMoves;
    [HideInInspector]
    public List<Chessman> PlayerChessmansToMove = new List<Chessman>();
    private List<Chessman> ActiveEnemies = new List<Chessman>();
    


    private void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update () {

        UpdateSelection();
        DrawChessboard();

        if (Input.GetMouseButtonDown(0))
        {
            if(selectionX >= 0 && selectionY >= 0)
            {
                if(selectedChessman == null)
                {
                    //Select the Chessman
                    SelectChessman(selectionX, selectionY);
                }
                else
                {
                    //move the chessman
                    MoveChessman(selectionX, selectionY);
                }
            }
        }

        //Check Player moves 
        //if(playerMovesLeft == 0 && isPlayerTurn)
        //{
        //    print("The player does't have any more moves");
        //    isPlayerTurn = !isPlayerTurn;
            
        //}
        if(PlayerChessmansToMove.Count == 0)
        {
            isPlayerTurn = false;
        }

    }

    private void SelectChessman(int x,int y)
    {
        if (Chessmans[x, y] == null)
            return;
        if (Chessmans[x,y].isPlayer != isPlayerTurn)
            return;
        if (Chessmans[x, y].CanBeSelected == false)
            return;

        //Check the allowed moves
        allowedMoves = Chessmans[x, y].PossibleMove();

        selectedChessman = Chessmans[x, y];
        BoardHilights.Instance.HighlightAllowedMoves(allowedMoves);
    }

    private void MoveChessman(int x,int y)
    {

        if (allowedMoves[x,y])
        {

            Chessman c = Chessmans[x, y];
            if (c != null && !c.isPlayer)
            {

                //Get Type
                if (c.GetType() == typeof(CommandUnit))
                {
                    //End Game
                    return;
                }
                
                //Attack the Enemy Unit

                c.hitPoints -= selectedChessman.attackPower;
                c.HP.text = c.hitPoints.ToString();
                if(c.hitPoints <= 0)
                {
                    activeChessman.Remove(c.gameObject);
                    ActiveEnemies.Remove(c);
                    Destroy(c.gameObject);
                }
                //Destroy that game object
                //activeChessman.Remove(c.gameObject);
                //Destroy(c.gameObject);
            }
            else
            {

                Chessmans[selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
                //Move the chessman
                selectedChessman.transform.position = tiles[x, y].gameObject.transform.position;
                selectedChessman.SetPostion(x, y);

                Chessmans[x, y] = selectedChessman;

                Chessmans[x, y].CanBeSelected = false;
                //End Turn
                //isPlayerTurn = !isPlayerTurn;
                UpdatePlayersMovesLeft(selectedChessman);
            }
        }

        BoardHilights.Instance.HideHighlights();

        selectedChessman = null;

    }

    void UpdatePlayersMovesLeft(Chessman c)
    {

        for (int i = 0; i < PlayerChessmansToMove.Count; i++)
        {
            if (PlayerChessmansToMove.Contains(c))
            {
                PlayerChessmansToMove.Remove(c);
            }

        }

        //print("Player has " + PlayerChessmansToMove.Count + " moves");

    }

    void ResetPlayerMoves()
    {
        PlayerChessmansToMove.Clear();

        for (int i = 0; i < activeChessman.Count; i++)
        {
            if (activeChessman[i].GetComponent<Chessman>().isPlayer)
            {
                PlayerChessmansToMove.Add(activeChessman[i].GetComponent<Chessman>());
            }
        }

        for (int i = 0; i < PlayerChessmansToMove.Count; i++)
        {
            PlayerChessmansToMove[i].CanBeSelected = true;
        }

        for(int i = 0; i < ActiveEnemies.Count; i++)
        {
            ActiveEnemies[i].CanBeSelected = true;
        }


        //print("Player has " + PlayerChessmansToMove.Count + " moves");
    }

    public void Generate()
    {
        tiles = new Tile[sizeX, sizeZ];

        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {

                CreateTile(x, z);
                
                
            }
        }
    }

    private void CreateTile(int x, int z)
    {

        Tile tile = Instantiate(tilePrefab) as Tile;

        tiles[x, z] = tile;
        tile.name = "Tile " + x + "," + z;
        tile.transform.parent = transform;
        tile.transform.localPosition = new Vector3(x - sizeX * 0.5f + 0.5f, 0f, z - sizeZ * 0.5f + 0.5f);

    }

    public void SpawnPieces(int Mode)
    {
        activeChessman = new List<GameObject>();
        Chessmans = new Chessman[8, 8];
        /* Modes 
         * 1 = Easy
         * 2 = Medium
         * 3 = Hard
         */

        //Easy Mode

        /** Enemy Pieces **/

        /** Command Unit **/

        SpawnChessman(0, 4, 7,Quaternion.Euler(-105f, 0f, 180),0,5);

        //GameObject CUnit = Instantiate(CommandUnitPrefab, tiles[4, 7].gameObject.transform.position, Quaternion.identity) as GameObject;
        //CUnit.transform.position = new Vector3(CUnit.transform.position.x, CUnit.transform.position.y + 0.25f, CUnit.transform.position.z);
        //CUnit.GetComponent<CommandUnit>().SetPostion(4, 7);

        /** Dreadnought Unit **/

        SpawnChessman(1, 3, 7, Quaternion.Euler(0, 180.0f, 0),2,5);

        //GameObject DreadnoughtUnit = Instantiate(DronePrefab, tiles[3, 7].gameObject.transform.position, Quaternion.identity) as GameObject;
        //DreadnoughtUnit.transform.position = new Vector3(DreadnoughtUnit.transform.position.x, DreadnoughtUnit.transform.position.y + 0.25f, DreadnoughtUnit.transform.position.z);
        //DreadnoughtUnit.GetComponent<Dreadnought>().SetPostion(3, 7);

        /** Drone Unit **/

        SpawnChessman(2, 5, 7, Quaternion.Euler(0, 0, 0),1,2);
        SpawnChessman(2, 6, 7, Quaternion.Euler(0, 0, 0),1,2);
        SpawnChessman(2, 2, 7, Quaternion.Euler(0, 0, 0),1,2);

        //GameObject DroneUnit = Instantiate(DreadnoughtPrefab, tiles[5, 7].gameObject.transform.position, Quaternion.identity) as GameObject;
        //DroneUnit.transform.position = new Vector3(DroneUnit.transform.position.x, DroneUnit.transform.position.y + 0.25f, DroneUnit.transform.position.z);

        /** Player pieces **/

        /** Grunt **/

        for (int x = 0; x < sizeX; x++)
        {
            SpawnChessman(3, x, 1, Quaternion.Euler(0, 180, 0),1,2);
            //GameObject GruntUnit = Instantiate(GruntPrefab, tiles[x, 1].gameObject.transform.position, Quaternion.Euler(0, 180, 0)) as GameObject;
            //GruntUnit.transform.position = new Vector3(GruntUnit.transform.position.x, GruntUnit.transform.position.y + 0.25f, GruntUnit.transform.position.z);
        }


        /** Jumpship Unit **/
        SpawnChessman(4, 3, 0, Quaternion.Euler(0, 0, 0),2,2);

        //GameObject JumpshipUnit = Instantiate(JumpshipPrefab, tiles[3, 0].gameObject.transform.position, Quaternion.identity) as GameObject;
        //JumpshipUnit.transform.position = new Vector3(JumpshipUnit.transform.position.x, JumpshipUnit.transform.position.y + 0.25f, JumpshipUnit.transform.position.z);

        /** Tank Unit **/
        SpawnChessman(5, 4, 0, Quaternion.Euler(0, 0, 0),2,4);
        //GameObject TankUnit = Instantiate(TankPrefab, tiles[4, 0].gameObject.transform.position, Quaternion.identity) as GameObject;
        //TankUnit.transform.position = new Vector3(TankUnit.transform.position.x, TankUnit.transform.position.y + 0.25f, TankUnit.transform.position.z);

        //for (int i = 0; i < activeChessman.Count; i++)
        //{

        //    if (activeChessman[i].GetComponent<Chessman>().isPlayer)
        //        playerMaxMoves++;

        //}

        //playerMovesLeft = playerMaxMoves;
        playerMaxMoves = PlayerChessmansToMove.Count;
        playerMovesLeft = playerMaxMoves;
        print("Player has " + playerMaxMoves + " moves");

    }

    private void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {

            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;

        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }


    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heigthLine = Vector3.forward * 8;

        for (int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);

            for (int j = 0; j <= 8; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heigthLine);
            }

        }

        //Draw the selection
        if (selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(Vector3.forward * selectionY + Vector3.right * selectionX,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));

            Debug.DrawLine(Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
                Vector3.forward * selectionY + Vector3.right * (selectionX + 1));

        }

    }

    private void SpawnChessman(int index, int x, int y, Quaternion rotation,int attPower, int HP)
    {
        GameObject piece = Instantiate(allPiecesPrefabs[index], tiles[x,y].gameObject.transform.position,rotation);
        piece.transform.SetParent(transform);
        Chessmans[x, y] = piece.GetComponent<Chessman>();
        Chessmans[x, y].SetPostion(x, y);
        Chessmans[x, y].CanBeSelected = true;
        Chessmans[x, y].attackPower = attPower;
        Chessmans[x, y].hitPoints = HP;
        Chessmans[x, y].HP.text = HP.ToString();

        activeChessman.Add(piece);

        if (piece.GetComponent<Chessman>().isPlayer)
            PlayerChessmansToMove.Add(piece.GetComponent<Chessman>());
        else
        {
            ActiveEnemies.Add(piece.GetComponent<Chessman>());
        }
    }


    /** AI Moves and Behaviour **/

    #region AI and Behaviuor


    public void AI_Moves()
    {

        for (int droneIndex = 0; droneIndex < ActiveEnemies.Count; droneIndex++)
        {
            if (ActiveEnemies[droneIndex].GetType() == typeof(Drone) && ActiveEnemies[droneIndex].CanBeSelected)
            {
                StartCoroutine( MoveDrone(ActiveEnemies[droneIndex]) );
                break;
            }
        }

        //GameObject[] dreadnoughts = GameObject.FindGameObjectsWithTag("Dreadnought");

        
        // Move the Dreadnoughts
        for (int dIndex = 0; dIndex < ActiveEnemies.Count; dIndex++)
        {
            //MoveDreadnoughts(dreadnoughts[dIndex].GetComponent<Chessman>());
            if (ActiveEnemies[dIndex].GetType() == typeof(Dreadnought) && ActiveEnemies[dIndex].CanBeSelected)
            {
                StartCoroutine( MoveDreadnoughts(ActiveEnemies[dIndex]) );
                break;
            }
        }



        // Move the Command Unit

        for (int cUnit = 0; cUnit < ActiveEnemies.Count; cUnit++)
        {
            //MoveDreadnoughts(dreadnoughts[dIndex].GetComponent<Chessman>());
            if (ActiveEnemies[cUnit].GetType() == typeof(CommandUnit) && ActiveEnemies[cUnit].CanBeSelected)
            {
                StartCoroutine( MoveCommandUnit(ActiveEnemies[cUnit]));
                break;
            }
        }

        int tempCounter = 0;
        for(int i = 0; i < ActiveEnemies.Count; i++)
        {
            if (!ActiveEnemies[i].CanBeSelected)
            {
                tempCounter++;
            }
        }
        if(tempCounter == ActiveEnemies.Count)
        {
            isPlayerTurn = true;
            ResetPlayerMoves();
        }



    }

    // First Drone Move and Attack

    IEnumerator MoveDrone(Chessman drone)
    {
        Chessman c;

        yield return new WaitForSeconds(3);

        //Check if it is inside the board
        if (drone.CurrentY > 0 && drone.CurrentY < 8)
        {
            c = Chessmans[drone.CurrentX, drone.CurrentY - 1];
            //Check infront
            if (c == null)
            {
               
                Chessmans[drone.CurrentX, drone.CurrentY] = null;
                //Move the chessman
                drone.transform.position = tiles[drone.CurrentX, drone.CurrentY - 1].gameObject.transform.position;
                drone.SetPostion(drone.CurrentX, drone.CurrentY - 1);

                Chessmans[drone.CurrentX, drone.CurrentY] = drone;
            }       

        }

        //Check for target

        //Diagonal Back Left
        if (drone.CurrentX != 0 && drone.CurrentY != 7)
        {
            c = Chessmans[drone.CurrentX - 1, drone.CurrentY + 1];
            if (c != null && c.isPlayer)
            {
                //Shoot
                //print("Shoot");
                c.hitPoints -= drone.attackPower;
                c.HP.text = c.hitPoints.ToString();
                if(c.hitPoints <= 0)
                {
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }
            }
        }

        //Diagonal Back Right
        if (drone.CurrentX != 7 && drone.CurrentY != 7)
        {
            c = Chessmans[drone.CurrentX + 1, drone.CurrentY + 1];
            if (c != null && c.isPlayer)
            {
                //Shoot
                //print("Shoot");
                c.hitPoints -= drone.attackPower;
                c.HP.text = c.hitPoints.ToString();
                if (c.hitPoints <= 0)
                {
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }
            }
        }

        //Diagonal Front Right
        if (drone.CurrentX != 7 && drone.CurrentY != 0)
        {
            c = Chessmans[drone.CurrentX + 1, drone.CurrentY - 1];
            if (c != null && c.isPlayer)
            {
                //Shoot
                //print("Shoot");
                c.hitPoints -= drone.attackPower;
                c.HP.text = c.hitPoints.ToString();
                if (c.hitPoints <= 0)
                {
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }
            }
        }

        //Diagonal Front Right
        if (drone.CurrentX != 0 && drone.CurrentY != 0)
        {
            c = Chessmans[drone.CurrentX - 1, drone.CurrentY - 1];
            if (c != null && c.isPlayer)
            {
                //Shoot
                //print("Shoot");
                c.hitPoints -= drone.attackPower;
                c.HP.text = c.hitPoints.ToString();
                if (c.hitPoints <= 0)
                {
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
                }
            }
        }

        drone.CanBeSelected = false;

        AI_Moves();

        StopAllCoroutines();

    }

    IEnumerator MoveDreadnoughts(Chessman dreadnought)
    {

        Chessman c, Enemy = null;

        yield return new WaitForSeconds(3);

        //Find direction to the enemy
        for (int x = 0; x < 8; x++)
        {
            for(int z = 0; z < 8; z++)
            {

                if (Chessmans[x, z] != null && Chessmans[x, z].isPlayer)
                {
                    if(Enemy == null)
                        Enemy = Chessmans[x, z];

                    if (Enemy.CurrentY < Chessmans[x, z].CurrentY)
                    {
                        Enemy = Chessmans[x, z];
                    }
                }
                    
            }
        }
        //print("I Found " + Enemy.CurrentX + " , " + Enemy.CurrentY);
        
        //Forward
        if (dreadnought.CurrentY > Enemy.CurrentY)
        {
            if (dreadnought.CurrentY > 0 && dreadnought.CurrentY < 8)
            {
                c = Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1];
                //Check infront
                if (c == null)
                {
                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = null;
                    //Move the chessman
                    dreadnought.transform.position = tiles[dreadnought.CurrentX, dreadnought.CurrentY - 1].gameObject.transform.position;
                    dreadnought.SetPostion(dreadnought.CurrentX, dreadnought.CurrentY - 1);

                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = dreadnought;
                }

            }

        }
        //Back
        if (dreadnought.CurrentY < Enemy.CurrentY)
        {
            if (dreadnought.CurrentY > 0 && dreadnought.CurrentY < 8)
            {
                c = Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1];
                //Check infront
                if (c == null)
                {
                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = null;
                    //Move the chessman
                    dreadnought.transform.position = tiles[dreadnought.CurrentX, dreadnought.CurrentY + 1].gameObject.transform.position;
                    dreadnought.SetPostion(dreadnought.CurrentX, dreadnought.CurrentY + 1);

                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = dreadnought;
                }

            }

        }


        //Right
        if (dreadnought.CurrentX < Enemy.CurrentX)
        {
            //Forward
            if (dreadnought.CurrentX > 0 && dreadnought.CurrentX < 8)
            {
                c = Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY];
                //Check infront
                if (c == null)
                {
                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = null;
                    //Move the chessman
                    dreadnought.transform.position = tiles[dreadnought.CurrentX + 1, dreadnought.CurrentY].gameObject.transform.position;
                    dreadnought.SetPostion(dreadnought.CurrentX + 1, dreadnought.CurrentY);

                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = dreadnought;
                }

            }

        }
        //Left
        if (dreadnought.CurrentX > Enemy.CurrentX)
        {
            //Forward
            if (dreadnought.CurrentX > 0 && dreadnought.CurrentX < 8)
            {
                c = Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY];
                //Check infront
                if (c == null)
                {
                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = null;
                    //Move the chessman
                    dreadnought.transform.position = tiles[dreadnought.CurrentX - 1, dreadnought.CurrentY].gameObject.transform.position;
                    dreadnought.SetPostion(dreadnought.CurrentX - 1, dreadnought.CurrentY);

                    Chessmans[dreadnought.CurrentX, dreadnought.CurrentY] = dreadnought;
                }

            }

        }

        // Attack

        if (dreadnought.CurrentX > 0 && dreadnought.CurrentX < 8 && dreadnought.CurrentY > 0 && dreadnought.CurrentY < 8)
        {
            //Left
            if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY] != null && Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].gameObject.name);
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].HP.text = Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY].gameObject);
                }
            }
            //Right
            if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY] != null && Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].gameObject.name);
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].HP.text = Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY].gameObject);
                }
            }
            //Up
            if (Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1] != null && Chessmans[dreadnought.CurrentX , dreadnought.CurrentY - 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX, dreadnought.CurrentY -1].gameObject.name);
                Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].HP.text = Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX, dreadnought.CurrentY - 1].gameObject);
                }
            }
            //Down
            if (Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1] != null && Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].gameObject.name);
                Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].HP.text = Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX, dreadnought.CurrentY + 1].gameObject);
                }
            }
            //UpLeft
            if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1] != null && Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].gameObject.name);
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].HP.text = Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY - 1].gameObject);
                }
            }
            //UpRight
            if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1] != null && Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].gameObject.name);
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].HP.text = Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY - 1].gameObject);
                }
            }
            //DownLeft
            if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1] != null && Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].gameObject.name);
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].HP.text = Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX - 1, dreadnought.CurrentY + 1].gameObject);
                }
            }
            //DownRight
            if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1] != null && Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].isPlayer)
            {
                //Shoot
                //print("Shoot " + Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].gameObject.name);
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].hitPoints -= dreadnought.attackPower;
                Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].HP.text = Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].hitPoints.ToString();
                if (Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].hitPoints <= 0)
                {
                    activeChessman.Remove(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].gameObject);
                    Destroy(Chessmans[dreadnought.CurrentX + 1, dreadnought.CurrentY + 1].gameObject);
                }
            }

        }

        dreadnought.CanBeSelected = false;

        AI_Moves();

        StopAllCoroutines();

    }

    IEnumerator MoveCommandUnit(Chessman CUnit)
    {

        yield return new WaitForSeconds(3);

        CUnit.CanBeSelected = false;
        AI_Moves();

        StopAllCoroutines();

    }

    #endregion
}
