﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class Pathfinding : MonoBehaviour {

    PathRequestManager requestManager;

    AGrid grid;

    //public Transform seeker, target;


    private void Awake()
    {
        requestManager = GetComponent<PathRequestManager>();
        grid = GetComponent<AGrid>();
    }

    //private void Update()
    //{
    //    if(Input.GetButtonDown("Jump"))
    //        FindPath(seeker.position, target.position);
    //}

    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {

        //Stopwatch sw = new Stopwatch();
        //sw.Start();

        Vector3[] waipoints = new Vector3[0];
        bool pathSuccess = false;

        ANode startNode = grid.NodeFromWolrdPoint(startPos);
        ANode targetNode = grid.NodeFromWolrdPoint(targetPos);

        if (startNode.walkable && targetNode.walkable)
        {



            Heap<ANode> openSet = new Heap<ANode>(grid.MaxSize);
            HashSet<ANode> closedSet = new HashSet<ANode>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                ANode currentNode = openSet.RemoveFirst();

                //ANode currentNode = openSet[0];
                //for(int i = 1; i < openSet.Count; i++)
                //{
                //    if(openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                //    {
                //        currentNode = openSet[i];
                //    }
                //}

                //openSet.Remove(currentNode);

                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    //sw.Stop();
                    //print("Path Found: " + sw.ElapsedMilliseconds + " ms");
                    pathSuccess = true;

                    break;
                }

                foreach (ANode neighbour in grid.GetNeighbours(currentNode))
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                        continue;

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);

                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }

                }

            }
        }

        yield return null;
        if(pathSuccess)
        {
            waipoints = RetracePath(startNode, targetNode); 
        }
        requestManager.FinishProcessingPath(waipoints, pathSuccess);
    }

    int GetDistance(ANode nodeA,ANode nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if(distX > distY)
            return 14 * distY + 10 * (distX - distY);
        return 14 * distX + 10 * (distY - distX);
    }

    Vector3[] RetracePath(ANode startNode, ANode endNode)
    {
        List<ANode> path = new List<ANode>();

        ANode currentNode = endNode;

        while(currentNode != startNode)
        {
            path.Add(currentNode);

            currentNode = currentNode.parent;
        }

        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);

        return waypoints;

    }

    Vector3[] SimplifyPath(List<ANode> path)
    {
        List<Vector3> waypoints = new List<Vector3>();

        Vector2 directionOld = Vector2.zero;

        for(int i = 1; i < path.Count;i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX,
                path[i - 1].gridY - path[i].gridY);
            if(directionNew != directionOld)
            {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
        }

        return waypoints.ToArray();
    }

    public void StartFindPath(Vector3 startPos, Vector3 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

}
