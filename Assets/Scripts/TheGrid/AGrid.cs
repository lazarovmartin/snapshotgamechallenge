﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGrid : MonoBehaviour {

    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    private ANode[,] grid;

    private float nodeDiameter;
    private int gridSizeX,gridSizeY;

    public bool displayGridGizmos;

    public int MaxSize
    {
        get { return gridSizeX * gridSizeY; }
    }

    private void Awake()
    {
        //Calc how many nodes we can fit  in the grid

        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        //Create the Grid
        CreateGrid();

    }

    //public List<ANode> path;

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        //if (onlyDisplayPathGizmos)
        //{
        //    if(path != null)
        //    {
        //        foreach (ANode n in path)
        //        {
        //            Gizmos.color = Color.black;
        //            Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
        //        }
        //    }
        //}
        //else
        //{
            if(grid != null && displayGridGizmos)
                    {
                        foreach(ANode node in grid)
                        {
                            Gizmos.color = (node.walkable) ? Color.white : Color.red;
                            //if(path != null)
                            //{
                            //    if (path.Contains(node))
                            //    {
                            //        Gizmos.color = Color.black;
                            //    }
                            //}
                            Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeDiameter - 0.1f));
                        }
             }
       // }

        

    }

    private void CreateGrid()
    {

        grid = new ANode[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 -
            Vector3.forward * gridWorldSize.y / 2;

        for(int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                //Detect each node in the space
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + 
                    Vector3.forward * (y * nodeDiameter + nodeRadius);

                //Collision Check
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius,unwalkableMask));

                //Create a new Node
                grid[x, y] = new ANode(walkable, worldPoint,x ,y);

            }
        }

    }

    public ANode NodeFromWolrdPoint(Vector3 worldPos)
    {
        float percentX = (worldPos.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPos.z + gridWorldSize.y / 2) / gridWorldSize.y;

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

        return grid[x,y];


    }

    public List<ANode> GetNeighbours(ANode node)
    {
        List<ANode> neighbours = new List<ANode>();

        for(int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if(checkX >= 0 && checkX < gridSizeX && checkY >=0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }

            }
        }

        return neighbours;

    }

}
