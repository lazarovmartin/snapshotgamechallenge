﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHilights : MonoBehaviour {

	public static BoardHilights Instance { set; get; }

    public GameObject highlightPrefab;
    private List<GameObject> highlights;


    private void Start()
    {
        Instance = this;
        highlights = new List<GameObject>();
    }

    private GameObject GetHighlightObject()
    {
        GameObject go = highlights.Find(g => !g.activeSelf);
        
        if(go == null)
        {
            go = Instantiate(highlightPrefab);
            highlights.Add(go);
        }

        return go;
    }

    public void HighlightAllowedMoves(bool[,] moves)
    {
        for(int x = 0; x < 8; x++)
        {
            for (int z = 0; z < 8; z++)
            {
                if (moves[x, z])
                {
                    GameObject go = GetHighlightObject();
                    go.SetActive(true);
                    go.transform.position = new Vector3(x + 0.5f, 0.1f, z + 0.5f);
                }
            }
        }
    }

    public  void HideHighlights()
    {
        foreach(GameObject go in highlights)
        {
            go.SetActive(false);
        }
    }

}
